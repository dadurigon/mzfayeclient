Pod::Spec.new do |s|
  s.name         = "MZFayeClient"
  s.version      = "1.0.1"
  s.summary      = "Faye Client for iOS and OSX. Supports subscription blocks."

  s.homepage     = "https://bitbucket.org/dadurigon/mzfayeclient"

  s.license      = { :type => 'MIT', :file => 'LICENSE' }

  s.author       = { "Michał Zaborowski" => "m1entus@gmail.com" }

  s.source       = { :git => "https://dadurigon@bitbucket.org/dadurigon/mzfayeclient.git", :tag => "1.0.1" }

  s.source_files = 'MZFayeClient/*.{h,m}'

  s.dependency 'SocketRocket', '~> 0.3.1-beta2'
  s.dependency 'Base64', '~> 1.0.1'

  s.platform     = :osx, '10.8'
  s.platform     = :ios, '8.0'


  s.framework  = 'QuartzCore'
  s.requires_arc = true


end
