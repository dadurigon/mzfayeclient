//
//  AppDelegate.swift
//  MZFayeClientMac
//
//  Created by Dion Durigon on 2015-09-08.
//  Copyright (c) 2015 Dion Durigon. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!


    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

